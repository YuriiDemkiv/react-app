import {Container, Fab, Paper, Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from "@material-ui/icons/Delete";
import React from "react";
import {deleteData, getData} from "../http/fetchData";
import {Redirect} from "react-router-dom";
import IconButton from "@material-ui/core/IconButton";

export class UserList extends React.Component {
	state = {
		error: null,
		users: [],
		isLoaded: false,
		addNew: false
	};
	handleAddNew = () => this.setState({
		addNew: true
	});

	deleteUser = (userId) => {
		deleteData(userId).then((res) => {
				if (res) {
					if (res['deletedCount'] === 1) {
						const list = this.state.users.filter(user => user._id !== userId);
						this.setState({
							users: list
						})
					} else if (res['deletedCount'] === this.state.users.length) {
						this.setState({
							users: []
						})
					}
				}
			}
		)
	};

	componentDidMount() {
		getData().then((users) => {
				this.setState({
					isLoaded: true,
					users: users
				});
			},
			(error) => {
				this.setState({
					isLoaded: true,
					error
				});
			});
	}

	render() {
		if (this.state.addNew) {
			return <Redirect to="/add-user"/>
		}
		return (
			<Container className="users">
				<Container maxWidth="lg" className="table-wrapper">
					<Paper>

						<Table>
							<TableHead>
								<TableRow>
									<TableCell align="center">ID</TableCell>
									<TableCell align="center">Name</TableCell>
									<TableCell align="center">Position</TableCell>
									<TableCell align="center">Birthdate</TableCell>
									<TableCell align="center">Description</TableCell>
									<TableCell align="center">Works remotely</TableCell>
									<TableCell align="center">Options</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{this.state.users.map(user =>
									<TableRow key={user._id}>
										<TableCell align="center">{user._id}</TableCell>
										<TableCell align="center">{user.name}</TableCell>
										<TableCell align="center">{user.position}</TableCell>
										<TableCell align="center">{user.birthdate}</TableCell>
										<TableCell align="center">{user.description}</TableCell>
										<TableCell align="center">{user.worksRemotely ? 'Yes' : 'No'}</TableCell>
										<TableCell align="center">
											<IconButton aria-label="delete" onClick={() => this.deleteUser(user._id)}>
												<DeleteIcon/>
											</IconButton>
										</TableCell>
									</TableRow>
								)}
							</TableBody>
						</Table>
					</Paper>

				</Container>

				<Fab color="primary" aria-label="add" onClick={this.handleAddNew} className="btn-add">
					<AddIcon/>
				</Fab>
				<Fab color="secondary" aria-label="delete" onClick={this.deleteUser} className="btn-delete-all">
					<DeleteIcon/>
				</Fab>

			</Container>
		)
	}
}
