import React from "react";
import {Container} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackOutlined from '@material-ui/icons/ArrowBackOutlined';
import {Redirect} from "react-router-dom";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import Switch from "@material-ui/core/Switch";
import {postData} from "../http/fetchData";
import MenuItem from "@material-ui/core/MenuItem";

export class AddUser extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			goBack: false,
			name: '',
			position: '',
			birthdate: '',
			description: '',
			worksRemotely: false,
			isDateEmpty: false
		};
		this.handleSubmit = this.handleSubmit.bind(this)
	}
	handleBackBtn = () => this.setState({goBack: true});


	handleCheckbox = name => event => {
		this.setState({...this.state, [name]: event.target.checked});
	};

	handleChange = name => event => {
		this.setState({...this.state, [name]: event.target.value});
	};

	handleDateChange = name => event => {
		this.setState({...this.state, [name]: event.format('DD.MM.YYYY')});
	};

	handleSubmit(event) {
		const data = {
			name: this.state.name,
			position: this.state.position,
			birthdate: this.state.birthdate,
			description: this.state.description,
			worksRemotely: this.state.worksRemotely
		};
		if (!data.birthdate.length) {
			return alert('Please select the date!')
		}
		event.preventDefault();
		postData(data).then((response) => {
			if (response && response._id) {
				this.setState({goBack: true})
			}
		});
	}

	render() {
		if (this.state.goBack) {
			return <Redirect to="/"/>
		}
		return (
			<Container maxWidth="lg" className="form-wrapper">
				<IconButton aria-label="back" className="back-btn" onClick={this.handleBackBtn}>
					<ArrowBackOutlined fontSize="inherit"/>
				</IconButton>
				<Container>
					<Paper className="paper-wrapper">
						<Container maxWidth="xs">
							<form onSubmit={this.handleSubmit}>
								<TextField
									label="Name"
									margin="normal"
									fullWidth={true}
									onChange={this.handleChange('name')}
									required
								/>

								<TextField
									select
									label="Position"
									value={this.state.position}
									onChange={this.handleChange('position')}
									margin="normal"
									fullWidth={true}
									align="left"
									required
								>
									<MenuItem key="qa" value="QA">
										QA
									</MenuItem>
									<MenuItem key="developer" value="Developer">
										Developer
									</MenuItem>
									<MenuItem key="manager" value="Manager">
										Manager
									</MenuItem>
								</TextField>
								<TextField
									label="Description"
									value={this.state.description}
									margin="normal"
									fullWidth={true}
									multiline={true}
									onChange={this.handleChange("description")}
									required
								/>
								<MuiPickersUtilsProvider utils={MomentUtils} className="date-picker">
									<DatePicker onChange={this.handleDateChange("birthdate")}
												value={new Date(this.state.birthdate)}
												format="DD.MM.YYYY"
												autoOk={true}
												fullWidth={true}
												initialFocusedDate={''}
												label="Birthdate"/>
								</MuiPickersUtilsProvider>
								<FormControlLabel control={
									<Switch
										checked={this.state.worksRemotely}
										onChange={this.handleCheckbox('worksRemotely')}
									/>
								} label="Works Remotely"
								/>

								<Button variant="contained"
										color="primary"
										fullWidth={true}
										type="submit">
									Submit
								</Button>
							</form>
						</Container>
					</Paper>
				</Container>
			</Container>
		);
	}
}
