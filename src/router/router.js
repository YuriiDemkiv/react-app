import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {AddUser} from "../add-new/add-user";
import {UserList} from "../users/user-list";

function AppRouter() {
	return (
		<Router>
			<Route path="/add-user/" component={AddUser}/>
			<Route path="/" exact component={UserList}/>
		</Router>
	);
}

export default AppRouter;
