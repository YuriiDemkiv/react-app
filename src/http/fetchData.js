const url = 'https://node-user-service.herokuapp.com/users';

export function postData(data = {}) {
	return fetch(url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json;charset=utf-8'
		}
	}).then(response => response.json())
}

export function getData(data = {}) {
	return fetch(url, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json;charset=utf-8'
		}
	}).then(response => response.json())
}

export function deleteData(userId = '') {
	const deleteUrl = userId.length ? url + '/' + userId : url;
	return fetch(deleteUrl, {
		method: 'DELETE'
	}).then(response => response.json())
}
