import React from 'react';
import './App.scss';
import {Box} from '@material-ui/core';
import AppRouter from "./router/router";
import MomentUtils from '@date-io/moment';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

function App() {
	return (
		<MuiPickersUtilsProvider utils={MomentUtils}>
			<Box className="App">
				<h3>React Test</h3>
				<AppRouter/>
			</Box>
		</MuiPickersUtilsProvider>
	);
}

export default App;
